# Mixing ADAM and SGD: a Combined Optimization Method
The MAS(Mixing ADAM and SGD) is a new optimizer that use togheder adam and sgd with a weighted value for each one. 
This value show the percentage of influence of this two optimizer. 

<p align="center">
  <img src="images/readme/egiptian_optimizer.png" width="350"  alt="intuitive image">
</p>

## Usage
* Install requirements
```bash
pip3 install -r requiremenst
```
* Train a toy example

```bash
cd scr/plots
python3 optimizer_toy_plots.py
```

* Train Image experiments

```bash
python3 src/train_multi_optimizer.py --path="/dataset/path" --lr=0.008 --batch-size="512" --momentum=0.95 --adam-w=1.0 --sgd-w=0.0 --gpu="0" --model="resnet18"
```

* Train Text experiments

```bash
CUDA_VISIBLE_DEVICES="0" python3 src/bert_train.py --path="/dataset/path" --dataset="cola" --lr="0.0002" --batch-size="100" --momentum="0.95" --adam-w="0.5" --sgd-w="0.5" --gpu="0" --max-epoch="50" 
```

# Results

<p align="center">
  <img src="images/readme/results.png" width="700"  alt="results tables">
</p>

# Citation
More details and results in published work:
```Bibtex
@article{lando2020mixing,
    title={Mixing ADAM and SGD: a Combined Optimization Method},
    author={Nicola Landro and Ignazio Gallo and Riccardo La Grassa},
    year={2020},
    journal={arXiv preprint},
    archivePrefix={arXiv},
}
```
