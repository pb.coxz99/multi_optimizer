#!/usr/bin/env bash

DATASET_PATH="/home/risen/datasets-nas/cifar100"
DATASET_NAME='cifar100'
RUN_NUMBER="4"
GPU="1"
MODEL="resnet18"
BATCH_SIZE="1024"

mkdir -p "logs/multi_optimizer/${DATASET_NAME}/${MODEL}"
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" --momentum=0.95 --adam-w=1.0 --sgd-w=0.0 --gpu="$GPU" --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/adam_$RUN_NUMBER.log"
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" --momentum=0.95 --adam-w=0.0 --sgd-w=1.0 --gpu="$GPU" --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/sgd_momentum0.95_$RUN_NUMBER.log"
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" --momentum=0.95 --adam-w=0.5 --sgd-w=0.5 --gpu="$GPU" --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_0.5_0.5_momentum0.95_$RUN_NUMBER.log"
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" --momentum=0.95 --adam-w=0.6 --sgd-w=0.4 --gpu="$GPU" --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_0.6_0.4_momentum0.95_$RUN_NUMBER.log"
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" --momentum=0.95 --adam-w=0.7 --sgd-w=0.3 --gpu="$GPU" --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_0.7_0.3_momentum0.95_$RUN_NUMBER.log"
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" --momentum=0.95 --adam-w=0.4 --sgd-w=0.6 --gpu="$GPU" --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_0.4_0.6_momentum0.95_$RUN_NUMBER.log"
