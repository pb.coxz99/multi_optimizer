import torch
EPOCHS = 2
LR = 0.1

x = torch.tensor([1.])
y = torch.tensor([5.])

model = torch.nn.Linear(1, 1)
criterion = torch.nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=LR)

for epoch in range(EPOCHS):
    pred = model(x)

    loss = criterion(y, pred)

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    print('EPOCH:', epoch, ', loss:', loss.item())
