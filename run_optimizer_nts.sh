#!/usr/bin/env bash

DATASET_PATH="/home/risen/datasets-nas/CUB_200_2011/CUB_200_2011"
SAVE_PATH="/home/risen/nic/multi_optimizer/logs"
RUN_NUMBER="1"
GPU="1"
EPOCHS=100

LR=0.0001
ADAM_W=0.3
SGD_W=0.7
MOMENTUM=0.9

python3.6 src/ntsnet_train.py --path="$DATASET_PATH" --save-dir="$SAVE_PATH" --lr="$LR" --momentum="$MOMENTUM" --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --max-epoch $EPOCHS > "logs/multi_optimizer/nts_${ADAM_W}_${SGD_W}_lr_${LR}_${RUN_NUMBER}.log"
